# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'referencia/version'

Gem::Specification.new do |spec|
  spec.name          = "referencia"
  spec.version       = Referencia::VERSION
  spec.authors       = ["Airam Lozano Alonso"]
  spec.email         = ["alu0100221879@ull.edu.es"]
  spec.description   = %q{Gema de la clase Referencia}
  spec.summary       = %q{Gema que guarda una referencia bibliográfica}
  spec.homepage      = "https://bitbucket.org/alu0100221879/prct06"
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
  spec.add_development_dependency "rspec", "~> 2.11"
end
