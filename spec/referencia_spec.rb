# encoding: utf-8

require "spec_helper"
require "referencia"


describe Referencia do
	
	before :each do		
		
		autores = ['Dave Thomas', 'Andy Hunt', 'Chad Fowler']
		titulo = 'Programming Ruby 1.9 & 2.0: The Pragmatic Programmers Guide'
		serie = '(The Facets of Ruby)'
		editorial = 'Pragmatic Bookshelf'
		edicion = 4
		fecha_publicacion = 'July 7, 2013'
		numeros_isbn = ['ISBN-13: 978-1937785499', 'ISBN-10: 1937785491']
		
		@r1 = Referencia::Referencia.new(autores, titulo, serie, editorial, edicion, fecha_publicacion, numeros_isbn)
	
	end
	
	describe "# Creación de un objeto Referencia" do
		it "Debe crearse un objeto Referencia" do
			expect(@r1).to be_instance_of(Referencia::Referencia)
		end
	end
	
	describe "# Atributos de Referencia" do
		
		it "Debe existir uno o más autores" do
			expect(@r1.autores.length).not_to eql(0)
		end
		
		it "Debe existir un título" do
			expect(@r1.titulo).not_to eql("")
		end
		
		it "Debe existir o no una serie" do
			# Nada. La serie puede tomar cualquier valor, incluso cadena vacía
		end
		
		it "Debe existir una editorial" do
			expect(@r1.editorial).not_to eql("")
		end
		
		it "Debe existir un número de edición" do
			expect(@r1.editorial).not_to eql(0)
		end
		
		it "Debe existir una fecha de publicación" do
			expect(@r1.fecha_publicacion).not_to eql("")
		end
		
		it "Debe existir uno o más números ISBN" do
			expect(@r1.numeros_isbn.length).not_to eql(0)
		end
	
	end
	
	describe "# Métodos de acceso" do
	
		it "Debe existir un método para obtener el listado de autores" do
			expect(@r1.autores).to eql(['Dave Thomas', 'Andy Hunt', 'Chad Fowler'])
		end
		
		it "Debe existir un método para obtener el título" do
			expect(@r1.titulo).to eql('Programming Ruby 1.9 & 2.0: The Pragmatic Programmers Guide')
		end
		
		it "Debe existir un método para obtener la serie" do
			expect(@r1.serie).to eql('(The Facets of Ruby)')
		end
		
		it "Debe existir un método para obtener la editorial" do
			expect(@r1.editorial).to eql('Pragmatic Bookshelf')
		end
		
		it "Debe existir un método para obtener el número de edición" do
			expect(@r1.edicion).to eql(4)
		end
		
		it "Debe existir una fecha de publicación" do
			expect(@r1.fecha_publicacion).to eql('July 7, 2013')
		end
		
		it "Debe existir un método para obtener el listado de ISBN" do
			expect(@r1.numeros_isbn).to eql(['ISBN-13: 978-1937785499', 'ISBN-10: 1937785491'])
		end
		
		it "Debe existir un método para obtener la referencia formateada" do
			expect(@r1).to respond_to(:to_s)
		end
	
	end

end
