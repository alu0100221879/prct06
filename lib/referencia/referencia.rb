# encoding: utf-8

module Referencia
	
	class Referencia
	
		attr_reader :autores, :titulo, :serie, :editorial, :edicion, :fecha_publicacion, :numeros_isbn
		
		def initialize(autores, titulo, serie, editorial, edicion, fecha_publicacion, numeros_isbn)
			@autores, @titulo, @serie, @editorial, @edicion, @fecha_publicacion, @numeros_isbn = autores, titulo, serie, editorial, edicion, fecha_publicacion, numeros_isbn
		end
		
		def to_s
			# Formato IEEE: Autores, Titulo del libro, Edicion. Lugar de publicación: Editorial, Año de publicación.
			# No se incluye el lugar de publicación porque no está entre los atributos
			s = ''
			@autores.each() { |a| s << a << ', '}
			s << "#{@titulo}, #{@edicion}ª edición. #{@editorial}, #{@fecha_publicacion}"
			return s
		end
		
	end
	
end